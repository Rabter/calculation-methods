function lab_1()
    debug_mode = true;
    maximize = true;
    costs = readmatrix("costs.txt");
    mat = costs(:,:);
    if debug_mode
        disp("Введенная матрица:");
        disp(mat);
    end

    if maximize
        greatest = max(max(mat));
        mat = greatest - mat;
        if debug_mode
            disp("Преобразованная матрица для решения задачи о максимизации:");
            disp(mat);
        end
    end

    mat = prepare_matrix(mat);

    if debug_mode
        disp("Матрица после подготовительного этапа:");
        disp(mat);
    end

    [marks, k] = IZS_dumb(mat);

    if debug_mode
        fprintf("Система независимых нулей (размер: %d):\n", k);
        display_full(mat, blanks(height(mat)), blanks(width(mat)), marks);
    end

    it = 0;
    while k < height(mat)
        it = it + 1;
        if debug_mode
            fprintf("k < n, улчучшаем СНН, итерация %d\n", it);
        end
        [mat, marks] = upgrade_IZS(mat, marks, debug_mode);
        k = 0;
        for i = 1:height(marks)
            for j = 1: width(marks)
                if marks(i, j) == '*'
                    k = k + 1;
                end
            end
        end
    end

    print_solution(costs, marks);

function [mat, marks] = upgrade_IZS(mat, marks, debug_mode)
    br = blanks(height(mat));
    bc = blanks(width(mat));
    for j = 1:width(marks)
        if mystrfind('*',transpose(marks(:,j))) > 0
            bc(j) = '+';
        end
    end

    if debug_mode
        disp("Заблокированные столбцы:");
        display_full(mat, br,bc, marks);
    end

    buildingLChain = true;
    while buildingLChain
        no_zero = true;
        i = 0;
        while i < height(mat) && no_zero
            i = i + 1;
            j = 0;
            while j < width(mat) && no_zero
                j = j + 1;
                if br(i) ~= '+' && bc(j) ~= '+' && mat(i,j) == 0 && marks(i,j) == ' '
                    no_zero = false;
                end
            end
        end

        if no_zero
            no_min = true;
            h = 0;
            for i = 1:height(mat)
                for j = 1:width(mat)
                    if isequal(br(i), bc(j), marks(i,j), ' ') && (no_min || mat(i, j) < h)
                        h = mat(i, j);
                        no_min = false;
                    end
                end
            end

            for j = 1:width(mat)
                if bc(j) == ' '
                    mat(:, j) = mat(:, j) - h;
                end
            end
            for i = 1:height(mat)
                if br(i) == '+'
                    mat(i, :) = mat(i, :) + h;
                end
            end

            if debug_mode
                fprintf("Среди невыделенных элементов нет нулей. Минимальный элемент h = %d\nПреобразованная матрица:\n", h);
                display_full(mat, br, bc, marks);
            end

        else
            marks(i,j) = "'";
    
            if debug_mode
                fprintf("Найден новый кандидат. Преобразованная матрица:\n");
                display_full(mat, br, bc, marks);
            end

            col = mystrfind('*',marks(i,:));
            if col > 0
                bc(col) = ' ';
                br(i) = '+';

                if debug_mode
                    fprintf("Переставляем выделение со столбца на строку:\n");
                    display_full(mat, br, bc, marks);
                end

            else
                [aps, stars] = build_L_chain(marks, i, j);
                for i = 1:height(aps)
                    marks(aps(i,1),aps(i,2)) = '*';
                end
                for i = 1:height(stars)
                    marks(stars(i,1),stars(i,2)) = ' ';
                end

                for i = 1:height(marks)
                    for j = 1:width(marks)
                        if marks(i, j) ~= '*'
                            marks(i, j) = ' ';
                        end
                    end
                end


                br = blanks(height(mat));
                bc = blanks(width(mat));

                if debug_mode
                    fprintf("Проводим замену по L-цепочке. Преобразованная матрица:\n");
                    display_full(mat, br, bc, marks);
                end

                buildingLChain = false;
            end
        end
    end

function [apostrophes, stars] = build_L_chain(marks, i, j)
    apostrophes = [];
    stars = [];
    if marks(i, j) == "'"
        apostrophes = [apostrophes; [i j]];
        nexti = mystrfind('*', transpose(marks(:,j)));
        nextj = -1;
        while nexti ~= 0 && nextj ~= 0
            nextj = mystrfind("'", marks(nexti,:));
            if nextj ~= 0
                stars = [stars; [nexti j]];
                apostrophes = [apostrophes; [nexti nextj]];
                nexti = mystrfind('*', transpose(marks(:,nextj)));
                j = nextj;
            end
        end
    end

function res = prepare_matrix(mat)
    res = mat;
    for i = 1:width(mat)
        res(:,i) = res(:,i)  - min(res(:,i));
    end
    for i = 1:height(mat)
        res(i,:) = res(i,:) - min(res(i,:));
    end

function display_full(mat, br, bc, marks)    
    for i = 1:width(bc)
        fprintf("%c  ", bc(i))
    end
    fprintf("\n");
    for i = 1:height(mat)
        for j = 1:width(mat)
            fprintf("%d%c ", mat(i,j), marks(i,j))
        end
        fprintf("%c\n", br(i))
    end
    fprintf("\n");

function print_solution(costs, marks)
    res = 0;
    disp("Оптимальное решение:");
    for i = 1:height(marks)
        for j = 1:width(marks)
            if marks(i,j) == '*'
                out = 1;
                res = res + costs(i,j);
            else
                out = 0;
            end
            fprintf("%d ", out)
        end
        fprintf("\n")
    end
    disp("Изначальная матрица:");
    for i = 1:height(costs)
        for j = 1:width(costs)
            fprintf("%d ", costs(i,j));
        end
        fprintf("\n")
    end
    fprintf("Итоговая стоимость: %d\n", res)

function [IZS, k] = IZS_dumb(mat)
    cols = true(1, width(mat));
    IZS = repmat(' ', size(mat));
    k = 0;
    i = 1;
    while i <= height(mat)
        j = 1;
        no_zero = true;
        while j <= width(mat) && no_zero
            if cols(j) && mat(i, j) == 0
                no_zero = false;
                IZS(i, j) = '*';
                k = k + 1;
                cols(j) = false;
            end
            j = j + 1;
        end
        i = i + 1;
    end

function i = mystrfind(sym, str)
    i = 0;
    no_sym = true;
    while i < width(str) && no_sym
        i = i + 1;
        if str(i) == sym
            no_sym = false;
        end
    end
    if no_sym
        i = 0;
    end

