function lab_2()
    global calls;
    calls = 0;
    a = 0;
    b = 1;
    eps = power(10, -6);
    l = b - a;
    t = (sqrt(5) - 1) / 2;
    x1 = b - t * l;
    x2 = a + t * l;
    f1 = f(x1);
    f2 = f(x2);
    
    fprintf("Iteration 1: x1 = %.10f x2 = %.10f\n", x1, x2)

    it = 2;
    points = [a b];
    while l > 2 * eps
        if f1 < f2
            b = x2;
            l = b - a;
            x2 = x1;
            f2 = f1;
            x1 = b - t * l;
            f1 = f(x1);
        else
            a = x1;
            l = b - a;
            x1 = x2;
            f1 = f2;
            x2 = a + t * l;
            f2 = f(x2);
        end
        points = [points; [a b]];
            fprintf("Iteration %d: x1 = %.10f  x2 = %.10f\n", it, x1, x2)
        it = it + 1;
    end
    x0 = (a + b) / 2;
    f0 = f(x0);
    fprintf("Xmin = %.10f, f(Xmin) = %.10f with %d calls\n", x0, f0, calls);
    draw(x0, f0, points);
end

function y=f(x)
    global calls;
    y = asin((35 .* x .* x - 30 .* x + 9) ./ 20) + cos((10 .* x.^3 + 185 .* x .* x + 340 * x + 103) ./ (50 .* x .* x + 100 .* x + 30))+ 0.5;
    calls = calls + 1;
end


function draw(xmin, fmin, points)
    figure(1)
    hold on
    x = 0:1e-3:1;
    y = f(x);
    ypoints = f(points);
    plot(x,y, 'blue-', xmin, fmin, 'red*', points, ypoints, 'black|');
    for i = 1:length(points)
        x1 = points(i, 1);
        x2 = points(i, 2);
        plot([x1 x2], [f(x1) f(x2)], 'red');
    end
    hold off
end
