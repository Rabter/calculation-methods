function lab_3()
    clc;
    global calls;
    calls = 0;
    a = 0;
    b = 1;
    eps =1e-6;

    if false
        l = b - a;
        t = (sqrt(5) - 1) / 2;
        x1 = b - t * l;
        x3 = a + t * l;
        f1 = f(x1);
        f3 = f(x3);
        if f1 < f3
            x2 = x3 - t * (x3 - a);
        else
            x2 = x1 + t * (b - x1);
        end
        f2 = f(x2);
    else
        x1 = a;
        x3 = b;
        x2 = (a + b)/2;
        f1 = f(x1);
        f2 = f(x2);
        f3 = f(x3);
    end

    cond = true;
    a1 = (f2 - f1) / (x2 - x1);
    a2 = ((f3 - f1) / (x3 - x1) - (f2 - f1) / (x2 - x1)) / (x3 - x2);
    xmin = (x1 + x2 - a1 / a2) / 2;
    fmin = f(xmin);

    if true
        fprintf("iteration 1: x1 = %.10f x2 = %.10f x3 = %.10f; xmin = %.10f\n", x1, x2, x3, xmin);
    end

    it = 2;

    while cond
        if xmin < x2
            if fmin > f2
                x1 = xmin;
                f1 = fmin;
            else
                x3 = x2;
                f3 = f2;
                x2 = xmin;
                f2 = fmin;
            end
        else
            if fmin > f2
                x3 = xmin;
                f3 = fmin;
            else
                x1 = x2;
                f1 = f2;
                x2 = xmin;
                f2 = fmin;
            end
        end

        a1 = (f2 - f1) / (x2 - x1);
        a2 = ((f3 - f1) / (x3 - x1) - (f2 - f1) / (x2 - x1)) / (x3 - x2);
        xold = xmin;
        xmin = (x1 + x2 - a1 / a2) / 2;
        fmin = f(xmin);
        cond = (abs(xold-xmin) >= eps);
        if true
            fprintf("Iteration %d: x1 = %.10f x2 = %.10f x3 = %.10f; xmin = %.10f\n", it, x1, x2, x3, xmin);
        end
        it = it + 1;
    end
    fprintf("Xmin = %.10f, f(Xmin) = %.10f with %d calls", xmin, fmin, calls);
    draw(xmin, fmin);
end


function y=f(x)
    global calls;
    y = asin((35*x*x - 30 * x + 9)/20) + cos((10 * power(x, 3) + 185 * x * x + 340 * x + 103) / (50 * x * x + 100 * x+ 30))+ 0.5;
    calls = calls + 1;
end

function draw(x0, f0)    
    figure
    x = 0:power(10, -3):1;
    y = arrayfun(@(a) f(a), x);
    plot(x,y, '-', x0, f0, '*');
end