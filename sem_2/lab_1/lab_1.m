function lab_1()
    clc;
    global calls;
    calls = 0;
    a = 0;
    b = 1;
    step = a - b;
    eps = power(10, -6);
    x0 = a;
    f0 = f(a);
    points = [];
    it  = 0;
    while abs(step) > eps
        points = [points x0];
        step = -step / 4;
        fprintf("iteration %d: x0=%.10f, f0=%.10f, delta=%.10f\n", it, x0, f0, step);
        it = it  + 1;
        x1 = x0 + step;
        f1 = f(x1);
        while f0 > f1 && a < x1 && x1 < b
            points = [points x0];
            x0 = x1;
            f0 = f1;
            x1 = x0 + step;
            f1 = f(x1);
        end
        x0 = x1;
        f0 = f1;
    end
    fprintf("Xmin = %.10f, f(Xmin) = %.10f with %d calls", x0, f0, calls);
    %x0 - 0.777
    draw(x0, f0, true, points);

function y=f(x)
    global calls;
    y = asin((35*x*x - 30 * x + 9)/20) + cos((10 * power(x, 3) + 185 * x * x + 340 * x + 103) / (50 * x * x + 100 * x+ 30))+ 0.5;
    %y = (x - 0.777).^6;
    calls = calls + 1;

function draw(x0, f0, flag, points)    
    figure
    x = 0:power(10, -3):1;
    y = arrayfun(@(a) f(a), x);
    if flag
        ypoints = arrayfun(@(a) f(a), points);
        plot(x,y, 'blue-', x0, f0, 'red*', points, ypoints, 'black|', points, f0, 'black|');
    else
        plot(x,y, '-', x0, f0, '*');
    end