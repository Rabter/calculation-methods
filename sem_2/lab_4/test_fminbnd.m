function test_fminbnd()
    clc;
    global calls;
    calls = 0;
    options = optimset('Display','iter','TolX', 1e-6);
    xmin = fminbnd(@(a) f(a), 0, 1, options);
    fprintf("Xmin = %.10f, f(Xmin) = %.10f with %d calls", xmin, f(xmin), calls - 1);

end

function y=f(x)
    global calls;
    y = asin((35*x*x - 30 * x + 9)/20) + cos((10 * power(x, 3) + 185 * x * x + 340 * x + 103) / (50 * x * x + 100 * x+ 30))+ 0.5;
    calls = calls + 1;
end