function lab_4()
    clc;
    global calls;
    calls = 0;
    a = 0;
    b = 1;
    eps = 1e-6;
    delta = 1e-6;

    xmin = (a + b) / 2;
    [f1, f2] = derivatives(xmin, delta);
    if true
        fprintf("Iteration 1: xmin = %.10f, fmin = %.10f\n", xmin, f(xmin));
        calls = calls - 1;
    end
    it = 2;

    cond = abs(f1) > eps;
    while cond
        xold = xmin;
        xmin = xmin - f1 / f2;
        [f1, f2] = derivatives(xmin, delta);
        if true
            fprintf("Iteration %d: xmin = %.10f, fmin = %.10f\n", it, xmin, f(xmin));
            calls = calls - 1;
        end
        it = it + 1;
        cond = abs(f1) > eps || abs(xold - xmin) > eps;
    end

    fprintf("Xmin = %.10f, f(Xmin) = %.10f with %d calls", xmin, f(xmin), calls - 1);
    draw(xmin, f(xmin));
end

function y=f(x)
    global calls;
    y = asin((35*x*x - 30 * x + 9)/20) + cos((10 * power(x, 3) + 185 * x * x + 340 * x + 103) / (50 * x * x + 100 * x+ 30))+ 0.5;
    calls = calls + 1;
end

function [first, second] = derivatives(x, delta)
    f2 = f(x + delta);
    f1 = f(x - delta);
    first = (f2 - f1) / (2 * delta);
    second = (f1 - 2 * f(x) + f2) / power(delta, 2);
end

function draw(x0, f0)    
    figure
    x = 0:power(10, -3):1;
    y = arrayfun(@(a) f(a), x);
    plot(x,y, '-', x0, f0, '*');
end